package ticTacToe;

import java.util.Scanner;

public class Application {

	private static String playerSymbol;
	private static boolean isGameWon = false;
	private static Komorka[][] tablicaKomorek = new Komorka[3][3];
	private static Scanner scan = new Scanner(System.in);
	private static String odpowiedz = "";

	public static void main(String[] args) {

		do {
			showBanner();
			createGrid();

			do {
				System.out.print("\tpodaj swoj symbol: ");
				playerSymbol = scan.next().toUpperCase();

				System.out.print("\tpodaj nr wiersza: ");
				int wiersz = scan.nextInt();

				System.out.print("\tpodaj nr kolumny: ");
				int kolumna = scan.nextInt();

				tablicaKomorek[wiersz][kolumna].setZnak(playerSymbol);
				tablicaKomorek[wiersz][kolumna].setWiersz(wiersz);
				tablicaKomorek[wiersz][kolumna].setKolumna(kolumna);
				tablicaKomorek[wiersz][kolumna].setOccupied(true);

				System.out.println(
						"\n\t*****Postawi�e�: " + playerSymbol + " w [ " + wiersz + " ]" + "[ " + kolumna + " ]");

				// drukujemy aktualny stan siatki do ticTacToe
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						System.out.print("\t" + tablicaKomorek[i][j].toString());
						if (j == 2) {
							System.out.println("");
						}
					}
				}
				// sprawdza czy jest ktoras z mozliwych wygranych

				if ((tablicaKomorek[0][0].getZnak().equals(tablicaKomorek[1][0].getZnak())
						&& tablicaKomorek[1][0].getZnak().equals(tablicaKomorek[2][0].getZnak())
						&& tablicaKomorek[2][0].getIsOccupied() == true)

						|| (tablicaKomorek[0][1].getZnak().equals(tablicaKomorek[1][1].getZnak())
								&& tablicaKomorek[1][1].getZnak().equals(tablicaKomorek[2][1].getZnak())
								&& tablicaKomorek[2][1].getIsOccupied() == true)

						|| (tablicaKomorek[0][2].getZnak().equals(tablicaKomorek[1][2].getZnak())
								&& tablicaKomorek[1][2].getZnak().equals(tablicaKomorek[2][2].getZnak())
								&& tablicaKomorek[2][2].getIsOccupied() == true)

						|| (tablicaKomorek[0][0].getZnak().equals(tablicaKomorek[0][1].getZnak())
								&& tablicaKomorek[0][1].getZnak().equals(tablicaKomorek[0][2].getZnak())
								&& tablicaKomorek[0][2].getIsOccupied() == true)

						|| (tablicaKomorek[1][0].getZnak().equals(tablicaKomorek[1][1].getZnak())
								&& tablicaKomorek[1][1].getZnak().equals(tablicaKomorek[1][2].getZnak())
								&& tablicaKomorek[1][2].getIsOccupied() == true)

						|| (tablicaKomorek[2][0].getZnak().equals(tablicaKomorek[2][1].getZnak())
								&& tablicaKomorek[2][1].getZnak().equals(tablicaKomorek[2][2].getZnak())
								&& tablicaKomorek[2][2].getIsOccupied() == true)

						|| (tablicaKomorek[0][0].getZnak().equals(tablicaKomorek[1][1].getZnak())
								&& tablicaKomorek[1][1].getZnak().equals(tablicaKomorek[2][2].getZnak())
								&& tablicaKomorek[2][2].getIsOccupied() == true)

						|| (tablicaKomorek[0][2].getZnak().equals(tablicaKomorek[1][1].getZnak())
								&& tablicaKomorek[1][1].equals(tablicaKomorek[2][0])
								&& tablicaKomorek[2][0].getIsOccupied() == true)) {

					isGameWon = true; // jak wygrana to zmieniamy na true
					System.out.println("\n\t**********************************************************************");
					System.out.println("\t**********************BRAWO**"+playerSymbol+"**Wygrale�!!!***************************");
					System.out.println("\t**********************************************************************");
					break;
				}
				if (	(  tablicaKomorek[0][0].getIsOccupied()==true && tablicaKomorek[1][0].getIsOccupied()==true
						   && tablicaKomorek[2][0].getIsOccupied()==true && tablicaKomorek[0][1].getIsOccupied()==true
						   && tablicaKomorek[1][1].getIsOccupied()==true && tablicaKomorek[2][1].getIsOccupied()==true
						   && tablicaKomorek[0][2].getIsOccupied()==true && tablicaKomorek[1][2].getIsOccupied()==true
						   && tablicaKomorek[2][2].getIsOccupied()==true)    )
				{
					// czyli wszystko zajete jednoczesnie
					System.out.println("\n\t******************************************************");
					System.out.println("\t******REMIS***REMIS***REMIS***REMIS***REMIS***REMIS*****");
					System.out.println("\t********************************************************");
					break;
				}

			} while (isGameWon != true);

			System.out.println("\n\n\tZakonczy�e� Gr�");
			System.out.println("\n\t" + "Czy grasz dalej [ yes/no ]: ");
			odpowiedz = scan.next();

			isGameWon = false;

		} while (odpowiedz.equals("yes"));

		System.out.println("By By ....");

	}// end main()
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static void createGrid() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				tablicaKomorek[i][j] = new Komorka(" ", false, i, j);
				System.out.print("\t" + tablicaKomorek[i][j].toString());
				if (j == 2) {
					System.out.println("");
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	private static void showBanner() {
		System.out.println("\n\n\n****************************************************************");
		System.out.println("*************************TIC*TAC*TOE****************************");
		System.out.println("****************************************************************");
	}

}// end class
