package ticTacToe;

public class Komorka {
	
	private String znak;
	private boolean isOccupied;
	
	private int wiersz;
	private int kolumna;
	
	
	public Komorka(String znak, boolean isOccupied, int wiersz, int kolumna ) {
		
		this.znak = znak;
		this.isOccupied = false;
		this.wiersz = wiersz;
		this.kolumna = kolumna;
	}
	
	public String getZnak() {
		return znak;
	}
	public void setZnak(String znak) {
		this.znak = znak;
	}
	
	public boolean getIsOccupied() {
		return isOccupied;
	}
	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}
	
	public int getWiersz() {
		return wiersz;
	}
	public void setWiersz(int wiersz) {
		this.wiersz = wiersz;
	}
	
	public int getKolumna() {
		return kolumna;
	}
	public void setKolumna(int kolumna) {
		this.kolumna = kolumna;
	}

	@Override
	public String toString() {
		return " [ "+ znak+" ]" + ":"+isOccupied+"   ";
	}
}
